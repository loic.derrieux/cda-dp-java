package fr.fondes.beweb.cda.basics;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import fr.fondes.beweb.cda.basics.core.implementations.IOFile;
import fr.fondes.beweb.cda.basics.core.implementations.IOStandard;
import fr.fondes.beweb.cda.basics.core.interfaces.Bindable;
import fr.fondes.beweb.cda.basics.core.interfaces.BindableInterface;
import fr.fondes.beweb.cda.basics.core.interfaces.IOInterface;
import fr.fondes.beweb.cda.basics.core.interfaces.Readable;
import fr.fondes.beweb.cda.basics.core.interfaces.Writable;
import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.EmptyException;
import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.ErrorException;
import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.LockedException;

public class App {
    public static void main(String[] args) throws Exception {

      List<Thread> threads = new ArrayList<>();

      for (int i = 0; i < 50; i++) {
        threads.add(new Thread(new String("thread " + i)){
          @Override
          public void run() {
              try {
                Thread.sleep(5000);
                System.out.println("debut du thread " + this.getName());
                Thread.sleep(3000);
                
              } catch (InterruptedException e) {
                System.out.println("Fin du thread " + this.getName());
              }
          }
        });
      }
      
      System.out.println("Avant le lancement des threads");
      for(Thread t : threads){
        t.start();
      }
      System.out.println("Après le lancement des threads");
      Thread t = new Thread(){
        @Override
        public void run() {
            IOStandard io = new IOStandard();
            IOFile iof = new IOFile("./toto.fr");

            while(true){
              try {
                io.write(iof.read());
              } catch (LockedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
              } catch (EmptyException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
              } catch (ErrorException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
              }
              System.out.println("");
            }
        }
      };

      t.start();


      Thread t2 = new Thread(){
        @Override
        public void run() {
            IOStandard io = new IOStandard();
            IOFile iof = new IOFile("./toto.fr");

            while(true){
              try {
                iof.write(io.read());
              } catch (LockedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
              } catch (EmptyException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
              } catch (ErrorException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
              }
              System.out.println("");
            }
        }
      };

      t2.start();
     

    }



    private static void cdaSocket() throws Exception{
      ServerSocket s = new ServerSocket(114);

      Socket client = s.accept();

      System.out.println("connected");
      BufferedReader br = new BufferedReader(
        new InputStreamReader(client.getInputStream())
      );
      String m = "";
      while ((m = br.readLine()) != null){

        System.out.println(m);
      }
    }

    private static void websocket() throws Exception{
      ServerSocket s = new ServerSocket(114);


      Socket client = s.accept();

      BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
      BufferedWriter bw = new BufferedWriter(new PrintWriter(client.getOutputStream()));
      

      String m;
      String key = "";
      while((m = br.readLine()).hashCode() != 0){
        if(m.startsWith("Sec-WebSocket-Key: ")){
          
          key = m.substring(19).concat("258EAFA5-E914-47DA-95CA-C5AB0DC85B11");
          
          key = Base64.getEncoder().encodeToString(
            
            MessageDigest.getInstance("SHA-1").digest(key.getBytes("UTF-8"))
          );
          System.out.println("values : " + key);
        }
       
      }

      
      bw.write("HTTP/1.1 101 Switching Protocols \r\n");
      bw.write("Upgrade: websocket \r\n");  
      bw.write("Connection: Upgrade \r\n");  
      bw.write("Sec-WebSocket-Accept: " + key + "\r\n"); 
      bw.write("\r\n"); 
      bw.flush();

      System.err.println("fin du flux");

      String msg = "allez vous faire foutre";

      bw = new BufferedWriter(new PrintWriter(client.getOutputStream()));
      
      byte b = (Integer.valueOf(129)).byteValue();
      client.getOutputStream().write(b);
      b = (Integer.valueOf(msg.length())).byteValue();
      client.getOutputStream().write(b);
      client.getOutputStream().write(msg.getBytes("UTF-8"));
      client.getOutputStream().flush();
      Thread t = new Thread(){
        @Override
        public void run() {
            while(true){
              try {
                client.getInputStream().read();
                int length = Integer.parseUnsignedInt("01111111",2) & client.getInputStream().read();
                byte[] maskKey = new byte[4];
                for (int i = 0; i < 4; i++) {
                  maskKey[i] = Integer.valueOf(client.getInputStream().read()).byteValue();
                }
                byte[] result = new byte[length];
                for(int i=0; i < length ; i ++){
                  int j = i % 4;
                  int c = client.getInputStream().read() ^ maskKey[j];
                  result[i] = Integer.valueOf(c).byteValue();                 
                }
                System.out.println(new String(result, Charset.forName("UTF-8")));
               this.wait();
                
              } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
              } catch(InterruptedException iex){
                interrupt();
              } catch(IllegalMonitorStateException ex){
                System.exit(-1);
              }

            }
        }

      };
        t.start();
      


    }
}
