package fr.fondes.beweb.cda.basics.core.implementations;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

import fr.fondes.beweb.cda.basics.core.interfaces.IOInterface;
import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.EmptyException;
import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.ErrorException;
import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.LockedException;

public class IOStandard  extends IOInterface{


    @Override
    public void write(String content) throws LockedException {
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(this.os));
        
        try {
            bw.write(content);
            bw.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }

    @Override
    public void write(String content, boolean append) throws LockedException {
        // TODO Auto-generated method stub
        
    }

    @Override
    protected void initialize() {
        this.os = System.out;
        this.is = System.in;        
    }

    @Override
    protected void loadDatas(Object o) {
        
    }

@Override
public String read() throws EmptyException, ErrorException {
    return (new Scanner(this.is,"UTF-8")).nextLine();
}


    
}
