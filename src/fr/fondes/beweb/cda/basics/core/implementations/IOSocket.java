package fr.fondes.beweb.cda.basics.core.implementations;

import java.io.IOException;
import java.net.Socket;

import fr.fondes.beweb.cda.basics.core.interfaces.IOInterface;
import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.EmptyException;
import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.ErrorException;
import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.LockedException;

public class IOSocket extends IOInterface {

    @Override
    public String read() throws EmptyException, ErrorException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void write(String content) throws LockedException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void write(String content, boolean append) throws LockedException {
        // TODO Auto-generated method stub
        
    }

    @Override
    protected void initialize() {        
        try {
            this.os = (new Socket()).getOutputStream();
            this.is = (new Socket()).getInputStream();
        } catch (IOException e) {
        }
        
    }

    @Override
    protected void loadDatas(Object o) {
        // TODO Auto-generated method stub
        
    }
    
}
