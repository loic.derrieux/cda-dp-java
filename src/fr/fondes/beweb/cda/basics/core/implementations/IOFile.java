package fr.fondes.beweb.cda.basics.core.implementations;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import fr.fondes.beweb.cda.basics.core.interfaces.IOInterface;
import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.EmptyException;
import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.ErrorException;
import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.LockedException;

public class IOFile extends IOInterface {


    private File file ;


    public IOFile(String filename){
        super(filename);  
    }

    @Override
    protected void initialize() {
        try {
            this.is = new FileInputStream(this.file);
            this.os = new FileOutputStream(this.file,true);
        } catch (FileNotFoundException e) {}        
    }



    @Override
    public void write(String content) throws LockedException {
       BufferedWriter bw = new BufferedWriter(new PrintWriter(this.os));
       try {
        bw.write(content);
        bw.flush();
        bw.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    
        
    }

    @Override
    public void write(String content, boolean append) throws LockedException {
        // TODO Auto-generated method stub
        
    }

    @Override
    protected void loadDatas(Object o) {
        this.file = new File((String)o);
        
    }


    
}
