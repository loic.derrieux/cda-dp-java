package fr.fondes.beweb.cda.basics.core.interfaces;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.EmptyException;
import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.ErrorException;
import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.LockedException;

public abstract class IOInterface implements BindableInterface , Bindable{
    
    protected InputStream is ;

    protected OutputStream os;


    @Override
    public String read() throws EmptyException, ErrorException {
        BufferedReader br = new BufferedReader(new InputStreamReader(this.is));
        String result = "";
        String content = "";
        try {
            while((content = br.readLine()) != null){
                result += content;
            }

        } catch (IOException e) {
        }
        return result;
    }

    public IOInterface(){
        this.initialize();
    }

    
    public IOInterface(Object o){
        this.loadDatas(o);
        this.initialize();
    }

    protected abstract void initialize();


    protected abstract void loadDatas(Object o);


     @Override
    public void bindFrom(Readable r) {
        try {
            this.write(r.read());
        } catch (LockedException | EmptyException | ErrorException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }

    @Override
    public void bindTo(Writable w) {
        try {
            w.write(this.read());
        } catch (LockedException | EmptyException | ErrorException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }

    @Override
    public void bindBidirectionnal(BindableInterface bindable) {
        try {
            bindable.write(this.read());
            this.write(bindable.read());
        } catch (LockedException | EmptyException | ErrorException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
   

}
