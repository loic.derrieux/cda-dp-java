package fr.fondes.beweb.cda.basics.core.interfaces;

import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.EmptyException;
import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.ErrorException;

public interface Readable{
    
    public String read() throws EmptyException , ErrorException;
}
