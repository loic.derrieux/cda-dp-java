package fr.fondes.beweb.cda.basics.core.interfaces;

import fr.fondes.beweb.cda.basics.core.interfaces.exceptions.LockedException;

public interface Writable{
    
    /**
     * Ecrase le contenu du fichier
     * @param content
     * @throws LockedException
     */
    public void write(String content) throws LockedException;

    /**
     * Ecrase ou ajoute le contenu au fichier
     * 
     * @param content
     * @param append
     * @throws LockedException
     */
    public void write(String content, boolean append) throws LockedException;
}
