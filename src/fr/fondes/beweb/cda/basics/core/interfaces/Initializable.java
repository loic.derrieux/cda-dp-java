package fr.fondes.beweb.cda.basics.core.interfaces;

public interface Initializable {
    
    public void initialize();


    public void loadDatas(Object o);

}
