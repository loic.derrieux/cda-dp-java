package fr.fondes.beweb.cda.basics.core.interfaces;

public interface Bindable{
    public void bindTo(Writable w);

    public void bindFrom(Readable r);


    public void bindBidirectionnal(BindableInterface bindable);
}
